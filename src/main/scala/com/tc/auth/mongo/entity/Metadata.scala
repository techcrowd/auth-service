package com.tc.auth.mongo.entity

import java.util.Date

class Metadata {
  var createdAt: Date = _
  var updatedAt: Date = _
  var active: Boolean = true
}
