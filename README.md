# Auth Service
Auth service is a Scala Spring Boot (2.2.2) project. It uses OAuth 2.0 and JWT to provide access and refresh token. Service supports password, client, authorization code and implicit grant flow types to retrieve access token.

Project depends on Mongodb and uses in-memory for access token store.

## Postman API
Includes Api endpoints for Auth service
~~~
https://www.getpostman.com/collections/abf153ddc0acd70aea21
~~~

## Release Notes
### 1.0.0

- initial version with api endpoints for Auth service
- Api endpoints includes client, user and check token
- project setup uses in-memory token store to store access tokens and in-memory client details

### Upcoming Releases

- persisting access token store using database
- persisting client details using database
- improving authorization code and implicit grant flow types after frontend has been implemented