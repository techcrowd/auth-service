package com.tc.auth._config

import java.util

import com.tc.auth._auth.{AppUserDetails, AppUserDetailsService}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.oauth2.common.{DefaultOAuth2AccessToken, OAuth2AccessToken}
import org.springframework.security.oauth2.provider.token.AccessTokenConverter
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter
import org.springframework.security.oauth2.provider.{OAuth2Authentication, OAuth2Request}
import org.springframework.stereotype.Component

@Component
class CustomTokenEnhancer extends JwtAccessTokenConverter {

  @Autowired
  private var appUserDetailsService: AppUserDetailsService = _

  override def enhance(accessToken: OAuth2AccessToken, authentication: OAuth2Authentication): OAuth2AccessToken = {
    if (authentication.isClientOnly) this.getClientAccessToken(accessToken, authentication) else this.getUserAccessToken(accessToken, authentication)
  }

  override def extractAuthentication(map: util.Map[String, _]): OAuth2Authentication = {
    val parameters = new java.util.HashMap[String, String]()
    parameters.put(AccessTokenConverter.CLIENT_ID, map.get("client_id").toString)
    val scopes = new util.HashSet[String](map.get("scope").asInstanceOf[java.util.ArrayList[String]])
    val auds = new util.HashSet[String](map.get("aud").asInstanceOf[java.util.ArrayList[String]])

    var authorities = new util.ArrayList[SimpleGrantedAuthority]();
    var appUserDetails: AppUserDetails = null;
    if (map.get("accessTokenType").asInstanceOf[String].equalsIgnoreCase("user")) {
      authorities = appUserDetailsService.getAuthorities();
      appUserDetails = new AppUserDetails(map.get("user_name").toString, "N/A", new util.ArrayList[SimpleGrantedAuthority]());
      appUserDetails.id = map.get("userId").toString
      appUserDetails.fullName = map.get("fullName").toString
      appUserDetails.authorityList = authorities
    } else {
      appUserDetails = new AppUserDetails(map.get("client_id").toString, "N/A", new util.ArrayList[SimpleGrantedAuthority]());
    }

    val request = new OAuth2Request(
      parameters,
      map.get("client_id").toString,
      authorities,
      true,
      scopes,
      auds,
      null,
      null,
      null
    )

    val token = new UsernamePasswordAuthenticationToken(appUserDetails, "N/A", authorities)
    new OAuth2Authentication(request, token)
  }

  def getUserAccessToken(oauth2AccessToken: OAuth2AccessToken, authentication: OAuth2Authentication): OAuth2AccessToken = {
    val appUserDetails = authentication.getPrincipal.asInstanceOf[AppUserDetails]
    val userInfo = new java.util.HashMap[String, Object]()
    var accessToken = oauth2AccessToken
    userInfo.put("username", appUserDetails.getUsername)
    userInfo.put("userId", appUserDetails.id)
    userInfo.put("fullName", appUserDetails.fullName)
    userInfo.put("accessTokenType", "user")
    accessToken.asInstanceOf[DefaultOAuth2AccessToken].setAdditionalInformation(userInfo)

    val customAccessToken = new DefaultOAuth2AccessToken(accessToken)
    accessToken = super.enhance(customAccessToken, authentication)
    val userAuthoritiesInfo = new java.util.HashMap[String, Object](userInfo)
    userAuthoritiesInfo.put("authorities", appUserDetails.authorityList)
    val info = new java.util.HashMap[String, Object]()
    info.put("userInfo", userAuthoritiesInfo)
    accessToken.asInstanceOf[DefaultOAuth2AccessToken].setAdditionalInformation(info)
    accessToken
  }

  def getClientAccessToken(oauth2AccessToken: OAuth2AccessToken, authentication: OAuth2Authentication): OAuth2AccessToken = {
    var accessToken = oauth2AccessToken
    val info = new java.util.HashMap[String, Object]()
    info.put("accessTokenType", "client")
    (accessToken.asInstanceOf[DefaultOAuth2AccessToken]).setAdditionalInformation(info)
    val customAccessToken = new DefaultOAuth2AccessToken(accessToken)
    accessToken = super.enhance(customAccessToken, authentication)
    accessToken
  }
}
