package com.tc.auth._auth

import java.util

import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User

class AppUserDetails (
 username: String,
 password: String,
 authorities: util.Collection[_ <: SimpleGrantedAuthority]
) extends User(username, password, authorities) {
  var id: String = _
  var fullName: String = _
  var authorityList: util.Collection[_ <: SimpleGrantedAuthority] = _
}
