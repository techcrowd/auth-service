package com.tc.auth._config

import com.tc.auth._auth.AppUserDetailsService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.{Bean, Configuration}
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.{EnableWebSecurity, WebSecurityConfigurerAdapter}
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder

@Configuration
@EnableWebSecurity
class SecurityConfig extends WebSecurityConfigurerAdapter {
  val log = LoggerFactory.getLogger(this.getClass)
  @Autowired
  private var appUserDetailsService: AppUserDetailsService = _

  @Bean
  def passwordEncoder: PasswordEncoder = new BCryptPasswordEncoder()

  @Bean
  @throws[Exception]
  override def authenticationManagerBean: AuthenticationManager = super.authenticationManagerBean

  @throws[Exception]
  override def configure(auth: AuthenticationManagerBuilder): Unit = {
    auth.userDetailsService(appUserDetailsService).passwordEncoder(passwordEncoder)
  }

  @throws[Exception]
  override def configure(http: HttpSecurity): Unit = {
    http
      .csrf
      .disable
      .cors.and.sessionManagement
      .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
      .and()
      .authorizeRequests()
      .antMatchers("/oauth/**").permitAll
      .antMatchers("/oauth/authorize").permitAll()
      .anyRequest().authenticated();
  }
}
