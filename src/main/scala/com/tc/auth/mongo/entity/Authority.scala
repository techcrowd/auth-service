package com.tc.auth.mongo.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

@Document
class Authority extends Metadata {
 @Id
 var id: String = _;
 @Indexed(unique = true, sparse = true)
 var authority: String = _;
}
