package com.tc.auth.mongo.repository

import java.util.Optional

import com.tc.auth.mongo.entity.User
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
trait UserRepository extends MongoRepository[User, String] {
  def findByEmail(email: String): Optional[User]
}
